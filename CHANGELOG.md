## 1.5.2

### FIX

- 修复: 隐藏 `php` 的 `X-Powered-By` 返回头在 `mode:thinkphp` 未生效的问题

## 1.5.1

### UPDATE

- 隐藏了 `php` 的 `X-Powered-By` 返回头

## 1.5.0

### ADD

- 添加了 `add X-Frame-Options: SAMEORIGIN;` 请求头

### FIX

- 使用 `exec` 来执行 `nginx` 服务, 这样可以少一个进程

## 1.4.1

### FIX

- fix: `open() "/etc/nginx/conf.d/external/https.conf" failed` nginx include file 的文件必须存在,
  解决方案就是生成了一个空白的 `/etc/nginx/conf.d/external/https.conf` 文件
  

## 1.4.0

### ADD

- `https` 支持, 通过设置 `NGINX_HTTPS` 开启 `https` 端口,
  证书通过 `NGINX_CRT` 和 `NGINX_CRT` 这两个变量进行设置.    
  例子:
  ```yml
  services:
    proxy:
      image: shynome/nginx:1.4
      environment:
        NGINX_HTTPS: 'lo.shynome.com' 
        NGINX_CRT: |
          ---------- CRT -----------
          CONTENT
          ---------- CRT -----------
        NGINX_KEY: |
          ---------- KEY -----------
          CONTENT
          ---------- KEY -----------
  ```
  

- 额外的 `proxy` 配置支持, 一些额外的配置就可以放在这里, 说是 `proxy` 相关的, 但也可以写其他的配置    
  例子:
  ```yml
  services:
  proxy:
    image: shynome/nginx:1.4
    environment:
      # nginx http 跳到 https
      # docker compose yml 写 $ 需要替换成 $$ , 暂时我没有找到好办法解决
      NGINX_PROXY: |
        if ($$ssl_protocol = "") { return 301 https://$$host$$request_uri; }
  ```