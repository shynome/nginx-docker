#!/bin/sh

set -e

if [ w = w$NGINX_ROOT ]; then
  NGINX_ROOT="/etc/nginx/conf.d"
fi

# set nginx mode
if [ w = w$NGINX_MODE ]; then
  echo "env NGINX_MODE is required"
  exit 1
fi 

# 保证目录存在
mkdir -p "${NGINX_ROOT}/external/" "${NGINX_ROOT}/vhost/"

nginx_mode_file="${NGINX_ROOT}/external/app.conf"
nginx_common_file="${NGINX_ROOT}/external/common.conf"
cp "${NGINX_ROOT}/nginx_modes/${NGINX_MODE}.conf" $nginx_mode_file
cp "${NGINX_ROOT}/nginx_modes/common.conf" $nginx_common_file

echo "include ${nginx_mode_file};">${NGINX_ROOT}/vhost/mode.conf

sed s%{{FASTCGI}}%$FASTCGI%g -i $nginx_mode_file
sed s%{{ROOT}}%$ROOT%g -i $nginx_common_file

if [ w = w$XFrameOptions ]; then XFrameOptions="SAMEORIGIN"; fi
sed s%{{XFrameOptions}}%$XFrameOptions%g -i $nginx_common_file
[[ w'spa' == w$NGINX_MODE ]] && sed s%{{XFrameOptions}}%$XFrameOptions%g -i $nginx_mode_file

echo "$NGINX_PROXY">${NGINX_ROOT}/external/proxy.conf;

if [ w != w$NGINX_HTTPS ]; then
  cp ${NGINX_ROOT}/unit/https.conf ${NGINX_ROOT}/external/https.conf
  echo "$NGINX_CRT">${NGINX_ROOT}/external/ssl.crt
  echo "$NGINX_KEY">${NGINX_ROOT}/external/ssl.key
else
  # fix: nginx include file must be existed 
  echo "">${NGINX_ROOT}/external/https.conf
fi
