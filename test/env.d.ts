export default 'es6'
declare global {
  interface Env {
    NGINX_MODE?:    'php'|'proxy'|'spa'|'thinkphp'
    ROOT?:          string
    FASTCGI?:       string
    NGINX_ROOT?:    string
    XFrameOptions?: 'DENY'|'SAMEORIGIN'|'ALLOW-FROM uri'
    NGINX_PROXY?:   string
  }
}