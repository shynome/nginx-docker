
const assert = require('assert')
const fs = require('fs')
const path = require('path')
const shell = require('shelljs')

const { env } = require('.')
const { NGINX_ROOT } = env
env.ROOT = 'app/'
env.NGINX_MODE = 'spa'
env.FASTCGI = 'php_fpm:9000'
env.XFrameOptions = 'SAMEORIGIN'

function readfile(file='',replace=false){
  file = path.join(NGINX_ROOT,file)
  let content = fs.readFileSync(file,'utf8')
  if(replace){ content = content.replace(/\n$/,'') }
  return content
}

describe('nginx conf gen',()=>{

  describe('mode',()=>{
  
    let app_conf = '', common_conf = ''
      , src_app_conf = '', src_common_conf = readfile(`nginx_modes/common.conf`)

    const emptyEnv = /**@type {Env}*/({
      NGINX_MODE:null,
      FASTCGI: null,
      NGINX_PROXY: null,
    })
    /**@type { Env[] } */
    let test_cases = [
      { NGINX_MODE: "php", FASTCGI:'php_fpm:9000', },
      { NGINX_MODE: 'proxy', NGINX_PROXY: 'hello', },
      { NGINX_MODE: 'spa', },
      { NGINX_MODE: 'thinkphp', FASTCGI:'php_fpm:9000', },
    ]

    let i = 0
    beforeEach(()=>{
  
      shell.rm('-rf',`${NGINX_ROOT}/external`,`${NGINX_ROOT}/vhost`)
      Object.assign(process.env,emptyEnv)

      Object.assign(process.env,test_cases[i++])
      shell.exec(`sh ${path.join(__dirname,'../rootfs/scripts/nginx-init.sh')}`)
  
      // read conf
      app_conf = readfile('external/app.conf')
      common_conf = readfile('external/common.conf')
      src_app_conf = readfile(`nginx_modes/${env.NGINX_MODE}.conf`)
    })
    afterEach(()=>{
      shell.rm('-rf',`${NGINX_ROOT}/external`,`${NGINX_ROOT}/vhost`)
    })
  
    for( let item of test_cases ){

      it(`${item.NGINX_MODE}`, function () {
        
        assert.equal(
          common_conf,
          src_common_conf.replace('{{ROOT}}',env.ROOT).replace('{{XFrameOptions}}',env.XFrameOptions)
        )
        switch(item.NGINX_MODE){
          case 'php':
            assert.equal(
              app_conf,
              src_app_conf.replace('{{FASTCGI}}',env.FASTCGI)
            )
            break
          case 'proxy':
            assert.equal(
              readfile(`/external/proxy.conf`, true),
              env.NGINX_PROXY
            )
            break
          case 'spa':
            assert.equal(
              app_conf,
              src_app_conf.replace('{{XFrameOptions}}',env.XFrameOptions),
            )
            break
          case 'thinkphp':
            assert.equal(
              app_conf,
              src_app_conf.replace('{{FASTCGI}}',env.FASTCGI)
            )
            break
        }
      })

    }
  
  })
  
})