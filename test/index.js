/// <reference path="./env.d.ts" />

const path = require('path')

process.env.NGINX_ROOT = path.join(__dirname,'../rootfs/etc/nginx/conf.d')
// @ts-ignore
exports.env = /**@type {Env} */(new Proxy(process.env,{ get(target,key){ return target[key] === null ? '' : target[key] } }))
