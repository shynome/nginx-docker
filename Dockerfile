FROM nginx:stable-alpine

STOPSIGNAL SIGTERM
EXPOSE 80 443
WORKDIR /app

COPY rootfs /

RUN chmod +x /nginx-start /scripts/*.sh
ENV NGINX_MODE='spa' \
    FASTCGI='php_fpm:9000' \
    ROOT='/app/'

CMD [ "/nginx-start" ]
