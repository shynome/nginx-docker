# Options Env

env name      | default value     | desc 
------------- | ----------------- | -------
NGINX_MODE    | `spa`             | expect value: [ `spa`, `thinkphp`, `proxy`, `php` ]
FASTCGI       | `php_fpm:9000`    | php_fpm: `php_fpm:9000` 
NGINX_HTTPS   | ''                | 有值就开启 https 具体的看 [CHANGELOG](./CHANGELOG.md#1.4.0)
NGINX_KEY     | ''                | https crt file content
NGINX_CRT     | ''                | https key file content
NGINX_PROXY   | ''                | 额外的 nginx 的配置

# Usage

```sh
docker run -d \
  -p 80:80 \
  -e NGINX_MODE=spa \
  shynome/nginx
```